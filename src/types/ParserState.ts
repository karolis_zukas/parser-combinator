export interface ParserState {
  target: string | DataView;
  index: number;
  result: any;
  isError: boolean;
  error: string | null;
}
