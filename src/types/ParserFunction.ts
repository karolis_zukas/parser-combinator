import { ParserState } from "./ParserState";

export type ParserFunction = (parserArgs: ParserState) => ParserState;
