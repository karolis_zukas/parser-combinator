import { ParserState } from "src/types";

export const updateParserState: (
  state: ParserState,
  index: number,
  result: string | number
) => ParserState = (
  state: ParserState,
  index: number,
  result: string | number
) => ({
  ...state,
  index,
  result,
});
