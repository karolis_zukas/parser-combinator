import { ParserState } from "../types";

export const updateParserResult: (
  state: ParserState,
  result: any
) => ParserState = (state: ParserState, result: string) => ({
  ...state,
  result,
});
