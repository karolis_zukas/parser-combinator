import { ParserState } from "src/types";

export const updateParserError: (
  state: ParserState,
  errorMsg: string
) => ParserState = (state: ParserState, errorMsg: string) => ({
  ...state,
  isError: true,
  error: errorMsg,
});
