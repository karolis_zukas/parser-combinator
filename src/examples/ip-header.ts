import {
  bit,
  int,
  one,
  rawString,
  sequenceOf,
  succeed,
  tag,
  uint,
  zero,
} from "../parsers";

import fs from "fs";
import path from "path";

/*
  Version: 4
  Header length (IHL): 5 * 32 - bit = 20 bytes || 160 bits
  TOS: 0x00
  Total Length: 0x0044 (68 bytes)
  Identification: 0xad0b
  Flags and Fragments: 0x0000
  TTL: 0x40 (64 hops)
  Protocol: 0x11 (UPD)
  Header Checksum: 0x7272
  Source: 0xac1402fd (172.20.2.253)
  Destination: 0xac140006 (172.20.0.6)
*/

/*
  some of different ways to interpret binary numbers
  0101111001100001        One 16 bit number (24161)
  01011110 01100001       Two 8 bit numbers (94, 97), or ascii ^, and a.
  0101 1110 0110 0001     Four 4 bit numbers (5, 14, 6, 1)
  0 1 0 1 1 1 1 0 0 1 1 0 0 0 0 1 sixteen individual bits.
*/

const parser = sequenceOf([
  uint(4).map(tag("Version")),
  uint(4).map(tag("IHL")),
  uint(6).map(tag("DSCP")),
  uint(2).map(tag("ECN")),
  uint(16).map(tag("Total Lenght")),
  uint(16).map(tag("Identification")),
  uint(3).map(tag("Flags")),
  uint(13).map(tag("Fragment Offset")),
  uint(8).map(tag("TTL")),
  uint(8).map(tag("Protocol")),
  uint(16).map(tag("Header Checksum")),
  uint(32).map(tag("Source IP")),
  uint(32).map(tag("Destination IP")),
]).chain((res) => {
  if (res[1].value > 5) {
    const remainingBytes = Array.from({ length: res[1].value - 20 }, () =>
      uint(8)
    );
    return sequenceOf(remainingBytes).chain((remaining) => [
      ...res,
      tag("Options")(remaining),
    ]);
  }
  return succeed(res);
});

export const runBitExample = async () => {
  fs.readFile(path.join(__dirname, "./packet.bin"), null, (err, fileDump) => {
    if (err) {
      throw new Error(err + "");
    }

    const file = fileDump.buffer;
    const dataView = new DataView(file);
    const res = parser.run(dataView);
    console.log(res);
    return res;
  });
};
