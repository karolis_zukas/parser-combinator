/*
    Add: (+ 10 2)
    Subtract: (- 10 5)
    Multiply: (* 10 2)

    Nest calculations: (+ (* 10 2) (- (/ 50 3) 2))
*/

import { ParserState } from "src/types";
import { digits, str, choice, sequenceOf, between, lazy } from "../parsers";

const betweenBrackets = between(str("("), str(")"));

const numberParser = digits.map((x) => ({
  type: "number",
  value: Number(x),
}));

const operatorParser = choice([str("+"), str("-"), str("*"), str("/")]);

const expr = lazy(() => choice([numberParser, operationParser]));

const operationParser = betweenBrackets(
  sequenceOf([operatorParser, str(" "), expr, str(" "), expr])
).map((res: any[]) => ({
  type: "operation",
  value: {
    op: res[0],
    a: res[2],
    b: res[4],
  },
}));

const evaluate = (node: { type: string; value: any }): any => {
  if (node.type === "number") {
    return node.value;
  }

  if (node.type === "operation") {
    if (node.value.op === "+") {
      return evaluate(node.value.a) + evaluate(node.value.b);
    }
  }

  if (node.type === "operation") {
    if (node.value.op === "-") {
      return evaluate(node.value.a) - evaluate(node.value.b);
    }
  }

  if (node.type === "operation") {
    if (node.value.op === "*") {
      return evaluate(node.value.a) * evaluate(node.value.b);
    }
  }

  if (node.type === "operation") {
    if (node.value.op === "/") {
      return evaluate(node.value.a) / evaluate(node.value.b);
    }
  }
};

export const interpreter = (program: string) => {
  const parsingResult = expr.run(program);
  if (parsingResult.isError) {
    throw new Error("Invalid Program");
  }

  return evaluate(parsingResult.result);
};
