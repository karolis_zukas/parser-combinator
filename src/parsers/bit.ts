import { ParserState } from "src/types";
import { updateParserError, updateParserState } from "../util";
import { Parser } from "./Parser";

/**
 * Finds and return a bit in a given Uint8Array. Moves index by 1;
 */
export const bit: Parser = new Parser((parserState: ParserState) => {
  if (parserState.isError) {
    return parserState;
  }

  const target: DataView = parserState.target as DataView;

  const byteOffset = Math.floor(parserState.index / 8);

  if (byteOffset >= target.byteLength) {
    return updateParserError(parserState, "Bit: Unexpected end of input");
  }

  const byte = target.getUint8(byteOffset);
  const bitOffset = 7 - (parserState.index % 8);

  const result = (byte & (1 << bitOffset)) >> bitOffset;
  return updateParserState(parserState, parserState.index + 1, result);
});
