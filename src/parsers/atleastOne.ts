import { ParserState } from "../types";
import { updateParserError, updateParserResult } from "../util";
import { Parser } from "./Parser";

/**
 * Takes a parser and return all matching tokens.
 * Similar to many, but throws error if no matches found
 * @param parser Parser function
 * @returns new Parser with all matching tokens in state
 *
 * returns all matching parser states. In this case [132, 'a']
 * @tutorial many(choice(digits,str('a'))).run('134a');
 *
 * returns error, because no tokens match
 * @tutorial many(digits).run('abc');
 */
export const many: (parser: Parser) => Parser = (parser: Parser) =>
  new Parser(
    (parserState: any): ParserState => {
      if (parserState.isError) {
        return parserState;
      }

      let nextState = parserState;
      const results = [];
      let done = false;

      while (!done) {
        let testState = parser.parserStateTransformer(nextState);

        if (!nextState.isError) {
          results.push(testState.result);
          nextState = testState;
        } else {
          done = true;
        }
      }

      if (results.length === 0) {
        return updateParserError(
          parserState,
          `atleastOne: Unable to match any input at index ${parserState.index}`
        );
      }

      return updateParserResult(nextState, results);
    }
  );
