import { updateParserError, updateParserResult } from "../util";
import { ParserFunction, ParserState } from "../types";

export class Parser {
  public parserStateTransformer: ParserFunction;

  constructor(parserStateTransformer: ParserFunction) {
    this.parserStateTransformer = parserStateTransformer;
  }

  /**
   * Takes string token, and runs parser function on it.
   * @param target on which to run the parser function
   * @returns ParserState
   */
  run(target: string | DataView) {
    const initialState: ParserState = {
      target,
      index: 0,
      result: null,
      isError: false,
      error: null,
    };
    return this.parserStateTransformer(initialState);
  }

  /**
   * Takes modifier function and returns new Parser with modified parser state.
   * Returns same parser state if there is error in parser chain.
   * @param fn modifier function, which will run on each parser token
   * @returns new Parser with modified state
   */
  map<T>(fn: (value: T) => any): Parser {
    return new Parser((parserState: ParserState) => {
      const nextState = this.parserStateTransformer(parserState);

      if (nextState.isError) {
        return nextState;
      }

      return updateParserResult(nextState, fn(nextState.result));
    });
  }

  /**
   * Allows to chain multiple parsers, and flatten the results.
   * Also can be used, to select next parser based on previous parsing result
   * Returns error if there is error up the chain.
   * @param fn takes previous parsing result and should return new @param Parser.
   */
  chain(fn: (value: any) => any): Parser {
    return new Parser((parserState: ParserState) => {
      const nextState = this.parserStateTransformer(parserState);

      if (nextState.isError) {
        return nextState;
      }

      const nextParser = fn(nextState.result);
      return nextParser.parserStateTransformer(nextState);
    });
  }

  /**
   * Takes modifier function and returns new @param Parser with modified parser state,
   * if there are errors in parser chain.
   * is reverse of map().
   * @param fn modifier function, which will run on error
   * @returns new Parser with modified state
   */
  errorMap(fn: (msg: string, index: number) => any): Parser {
    return new Parser((parserState: ParserState) => {
      const nextState = this.parserStateTransformer(parserState);

      if (!nextState.isError) {
        return nextState;
      }

      return updateParserError(nextState, fn(nextState.error, nextState.index));
    });
  }
}
