import { updateParserError } from "../util";
import { Parser } from "./Parser";

export const fail = (errorMsg: string | number) =>
  new Parser((parserState) => {
    return updateParserError(parserState, errorMsg.toString());
  });
