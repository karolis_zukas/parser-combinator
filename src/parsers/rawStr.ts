import { uint } from ".";
import { bit } from "./bit";
import { fail } from "./fail";
import { sequenceOf } from "./sequenceOf";
import { succeed } from "./succeed";

/**
 * Match bytes to raw string
 * @param s string against which byte sequence will be matched.
 * @returns parser with sequence of bytes if string matches, or error if it does not.
 */
export const rawString = (s: string) => {
  if (s.length < 1) {
    throw new Error(`RawString: s must be at least 1 character`);
  }

  const byteParsers = s
    .split("")
    .map((c) => c.charCodeAt(0))
    .map((n: number) => {
      return uint(8).chain((res) => {
        if (res === n) {
          return succeed(n);
        } else {
          return fail(
            `RawString: Expected character ${String.fromCharCode(
              n
            )} but got ${String.fromCharCode(res)}`
          );
        }
      });
    });

  return sequenceOf(byteParsers);
};
