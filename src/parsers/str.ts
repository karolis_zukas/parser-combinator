import { updateParserState, updateParserError } from "../util";
import { Parser } from "./Parser";

export const str: (s: string) => Parser = (s: string) =>
  new Parser((parserState: any) => {
    const { target, index, isError } = parserState;

    if (isError) {
      return parserState;
    }

    const slicedTarget: string = target.slice(index);

    if (slicedTarget.length === 0) {
      return updateParserError(
        parserState,
        `str: Tried to match ${s}, but got unexpected end of input`
      );
    }

    if (target.slice(index).startsWith(s)) {
      return updateParserState(parserState, index + s.length, s);
    }

    return updateParserError(
      parserState,
      `str: Tried to match ${s}, but got ${target.slice(index, index + 20)}`
    );
  });
