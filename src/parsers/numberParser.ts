import { digits } from ".";

export const numberParser = digits.map((result) => ({
  type: "number",
  value: Number(result),
}));
