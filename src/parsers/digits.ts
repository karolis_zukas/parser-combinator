import { ParserState } from "src/types";
import { updateParserState, updateParserError } from "../util";
import { Parser } from "./Parser";

const digitsRegex = /^[0-9]+/;

export const digits: Parser = new Parser((parserState: ParserState) => {
  const { target, index, isError } = parserState;

  if (isError) {
    return parserState;
  }

  const slicedTarget: string = (target as string).slice(index);

  if (slicedTarget.length === 0) {
    return updateParserError(
      parserState,
      `digits: Got unexpected end of input`
    );
  }

  const regexMatch = slicedTarget.match(digitsRegex);

  if (regexMatch) {
    return updateParserState(
      parserState,
      index + regexMatch[0].length,
      regexMatch[0]
    );
  }

  return updateParserError(
    parserState,
    `digits: Could't match digits at index ${index}`
  );
});
