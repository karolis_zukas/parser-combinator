import { ParserState } from "../types";
import { updateParserError, updateParserResult } from "../util";
import { Parser } from "./Parser";

export const separatedByOne = (separatorParser: Parser) => (
  valueParser: Parser
) =>
  new Parser((parserState: ParserState) => {
    const results = [];
    let nextState: ParserState = parserState;

    while (true) {
      const thingWeWantState: ParserState = valueParser.parserStateTransformer(
        nextState
      );

      if (thingWeWantState.isError) {
        break;
      }
      results.push(thingWeWantState.result);
      nextState = thingWeWantState;

      const seperatorState = separatorParser.parserStateTransformer(nextState);
      if (seperatorState.isError) {
        break;
      }
      nextState = seperatorState;

      if (results.length === 0) {
        return updateParserError(
          parserState,
          `seperatedByOne: Unable to capture any result at index ${parserState.index}`
        );
      }
    }

    return updateParserResult(nextState, results);
  });
