import { updateParserResult } from "../util";
import { Parser } from "./Parser";

export const succeed = (value: string | number) =>
  new Parser((parserState) => {
    return updateParserResult(parserState, value);
  });
