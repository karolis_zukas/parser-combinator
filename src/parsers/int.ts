import { bit } from "./bit";
import { sequenceOf } from "./sequenceOf";

/**
 * Extract integer from byte sequence.
 * Uses Twos Compliment system.
 * Turning 1 to -1
 * Starting number
 * 0001
 * Flip all the bits and add 1
 * 1110 + 0001 = 1111 -> -1
 * @param n number of bits to read.
 * @returns parser with int number in state.
 */
export const int = (n: number) => {
  if (n < 1) {
    throw new Error(`Uint: n must be larger than 0, but we ${n}`);
  }

  if (n > 32) {
    throw new Error(`Uint: n must be less than 32, but we got ${n}`);
  }

  return sequenceOf(Array.from({ length: n }, () => bit)).map(
    (bits: number[]) => {
      if (bits[0] === 0) {
        return bits.reduce((acc, bit, i) => {
          return acc + Number(BigInt(bit) << BigInt(n - 1 - i));
        }, 0);
      } else {
        return -(
          1 +
          bits.reduce((acc, bit, i) => {
            return acc + Number(BigInt(bit === 0 ? 1 : 0) << BigInt(n - 1 - i));
          }, 0)
        );
      }
    }
  );
};
