import { ParserState } from "src/types";
import { updateParserState, updateParserError } from "../util";
import { Parser } from "./Parser";

const lettersRegex = /^[A-Za-z]+/;

export const letters: Parser = new Parser((parserState: ParserState) => {
  const { target, index, isError } = parserState;

  if (isError) {
    return parserState;
  }

  const slicedTarget: string = (target as string).slice(index);

  if (slicedTarget.length === 0) {
    return updateParserError(
      parserState,
      `letters: Got unexpected end of input`
    );
  }

  const regexMatch = slicedTarget.match(lettersRegex);

  if (regexMatch) {
    return updateParserState(
      parserState,
      index + regexMatch[0].length,
      regexMatch[0]
    );
  }

  return updateParserError(
    parserState,
    `letters: Could't match letters at index ${index}`
  );
});
