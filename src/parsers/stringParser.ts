import { letters } from "./letters";

export const stringParser = letters.map((result) => ({
  type: "string",
  value: result,
}));
