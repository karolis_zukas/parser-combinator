import { ParserState } from "../types";
import { updateParserResult } from "../util";
import { Parser } from "./Parser";

export const separatedBy = (separatorParser: Parser) => (valueParser: Parser) =>
  new Parser((parserState: ParserState) => {
    const results = [];
    let nextState: ParserState = parserState;

    if (parserState.isError) {
      return parserState;
    }

    while (true) {
      const thingWeWantState: ParserState = valueParser.parserStateTransformer(
        nextState
      );

      if (thingWeWantState.isError) {
        break;
      }
      results.push(thingWeWantState.result);
      nextState = thingWeWantState;

      const seperatorState = separatorParser.parserStateTransformer(nextState);
      if (seperatorState.isError) {
        break;
      }
      nextState = seperatorState;
    }

    return updateParserResult(nextState, results);
  });
