import { ParserState } from "src/types";
import { updateParserResult } from "../util/updateParserResult";
import { Parser } from "./Parser";

/**
 * Takes array of parsers and runs it in sequence
 * @param parsers array of parser functions
 * @returns new Parser with modified state
 *
 * Will return without errors:
 * @tutorial sequenceOf([digits,letters,digits]).run('1a2');
 *
 * Will return with errors, because the string is out of sequence:
 * @tutorial sequenceOf([letters,digits,letters]).run('1a2');
 */
export const sequenceOf: (parsers: Parser[]) => Parser = (parsers: Parser[]) =>
  new Parser(
    (parserState: any): ParserState => {
      if (parserState.isError) {
        return parserState;
      }

      const results = [];
      let nextState = parserState;

      for (const p of parsers) {
        nextState = p.parserStateTransformer(nextState);
        results.push(nextState.result);
      }

      if (nextState.isError) {
        return nextState;
      }

      return updateParserResult(nextState, results);
    }
  );
