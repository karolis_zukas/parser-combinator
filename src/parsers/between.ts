import { Parser } from "./Parser";
import { ParserState } from "../types";
import { sequenceOf } from "./sequenceOf";

/**
 * Takes left and right parser and returns a function
 * returned function takes one more parser (contentParser).
 * Parses the data in between left and right parsers.
 * @param left parser function
 * @param right parser function
 * @param contentParser parser function, which parses the data in between left and right parsers.
 * @returns new Parser with modified state
 *
 * Will return 'hello', because letters parser, parses the content in between the brackets in string 'hello'
 * @tutorial between(str("("),str(")"))(letters).run('(hello)')
 */
export const between = (leftParser: Parser, rightParser: Parser) => (
  contentParser: Parser
) =>
  sequenceOf([leftParser, contentParser, rightParser]).map(
    (result: ParserState[]) => result[1]
  );
