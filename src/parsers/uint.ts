import { bit } from "./bit";
import { sequenceOf } from "./sequenceOf";

/**
 * Extract unsigned integer from byte sequence
 * @param n number of bits to read.
 * @returns parser with uint number in state.
 */
export const uint = (n: number) => {
  if (n < 1) {
    throw new Error(`Uint: n must be larger than 0, but we ${n}`);
  }

  if (n > 32) {
    throw new Error(`Uint: n must be less than 32, but we got ${n}`);
  }

  return sequenceOf(Array.from({ length: n }, () => bit)).map((bits: []) => {
    return bits.reduce((acc, bit, i) => {
      return acc + Number(BigInt(bit) << BigInt(n - 1 - i));
    }, 0);
  });
};
