import { ParserState } from "../types";
import { updateParserResult } from "../util";
import { Parser } from "./Parser";

/**
 * Takes a parser and returns all matches.
 * Never fails, return 0 or more results
 * @param parser Parser function
 * @returns new Parser with all matching tokens in state
 *
 * returns all matching parser states. In this case ['a', 2, 'b', 3]
 * @tutorial many(choice([digits,letters])).run('a2b3');
 */
export const many: (parser: Parser) => Parser = (parser: Parser) =>
  new Parser(
    (parserState: any): ParserState => {
      if (parserState.isError) {
        return parserState;
      }

      let nextState = parserState;
      const results = [];
      let done = false;

      while (!done) {
        let testState = parser.parserStateTransformer(nextState);

        if (!testState.isError) {
          results.push(testState.result);
          nextState = testState;
        } else {
          done = true;
        }
      }

      return updateParserResult(nextState, results);
    }
  );
