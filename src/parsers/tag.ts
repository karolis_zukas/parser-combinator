export const tag = (type: string) => (value: any) => ({ type, value });
