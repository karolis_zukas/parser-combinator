import { ParserState } from "src/types";
import { Parser } from "./Parser";

/**
 * Is used to get around javascript eagerness,
 * by using thunks, whenever parsers are used recursivly, and are difined by each other.
 * @param parser
 * @returns new @param Parser
 *
 * Both parsers need to be difined at runtime, but they are circulary dependant.
 * @tutorial value=lazy(()=>choice([digits,arrayParser]));
 * @tutorial arrayParser=betweenBrackets(commaSeperatedValues(value));
 */
export const lazy = (parserThunk: () => Parser) =>
  new Parser((parserState: ParserState) => {
    const parser = parserThunk();
    return parser.parserStateTransformer(parserState);
  });
