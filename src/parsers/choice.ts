import { ParserState } from "src/types";
import { updateParserResult, updateParserError } from "../util";
import { Parser } from "./Parser";

/**
 * Takes array of parsers and ruturns state of first succesfull parser
 * or returns error if no matches were found
 * @param parsers array of parser functions
 * @returns new Parser with first succesful parser state
 *
 * returns first matching token, in this case letter 'a'.
 * @tutorial choice([letters,digits,letters]).run('1a2');
 *
 * returns error, as there are no matching tokens.
 * @tutorial choice([letters,letters]).run('123');
 */
export const choice: (parsers: Parser[]) => Parser = (parsers: Parser[]) =>
  new Parser(
    (parserState: any): ParserState => {
      if (parserState.isError) {
        return parserState;
      }

      for (const p of parsers) {
        const nextState = p.parserStateTransformer(parserState);

        if (!nextState.isError) {
          return nextState;
        }
      }

      return updateParserError(
        parserState,
        `choice: Unable to match with any parser at index ${parserState.index}`
      );
    }
  );
