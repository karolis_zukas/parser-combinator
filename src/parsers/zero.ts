import { ParserState } from "src/types";
import { updateParserError, updateParserState } from "../util";
import { Parser } from "./Parser";

/**
 * Finds and return a bit in a given Uint8Array. Moves index by 1;
 */
export const zero: Parser = new Parser((parserState: ParserState) => {
  if (parserState.isError) {
    return parserState;
  }

  const target: DataView = parserState.target as DataView;

  const byteOffset = Math.floor(parserState.index / 8);

  if (byteOffset >= target.byteLength) {
    return updateParserError(parserState, "Zero: Unexpected end of input");
  }

  const byte = target.getUint8(byteOffset);

  /**
   * Reverse the index, so that we would go from left to right
   */
  const bitOffset = 7 - (parserState.index % 8);

  /**
   * Find the required bit in byte
   */
  const result = (byte & (1 << bitOffset)) >> bitOffset;

  if (result !== 0) {
    return updateParserError(
      parserState,
      `Zero: expected 0 but got 1, at index ${parserState.index}`
    );
  }

  return updateParserState(parserState, parserState.index + 1, result);
});
