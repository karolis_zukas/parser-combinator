import {
  str,
  sequenceOf,
  letters,
  digits,
  choice,
  many,
  between,
  stringParser,
  numberParser,
  separatedBy,
  lazy,
} from "./src/parsers";
import { interpreter } from "./src/examples/micro-calculus";
import { runBitExample } from "./src/examples/ip-header";

const betweenBrackets = between(str("["), str("]"));
const commaSeperatedValues = separatedBy(str(","));

const value = lazy(() => choice([digits, arrayParser]));

const arrayParser = betweenBrackets(commaSeperatedValues(value));

const exapleString = "[1,[2,[3],4],5]";

// Micro-calculus
const program = "(+ (* 10 2) (- (/ 50 3) 2))";
//console.log(interpreter(program));

// Bits
runBitExample();
