### Parser combinators

http://theorangeduck.com/page/you-could-have-invented-parser-combinators

All the parsers should follow this signature:
`parser = ParserState in -> ParserState out`

## Combining parsers

### MANY

`many(choice([digits, letters])).run("1a234aa")`
Will return:
`result: [ '1', 'a', '234', 'aa' ]`

`many(digits).run("1ab2cg")`
Will return:
`result: [ '1' ]`

`many(sequenceOf([digits, letters])).run("12ab2cg")`
Will return:
`result: [ [ '12', 'ab' ], [ '2', 'cg' ] ]`

### SEQUENCEOF

Dice rolling parser:

```
const diceRollParser = sequenceOf([digits, str("d"), digits]).map(
  ([numOfDice, _, numOfSides]: string[]) => ({
    type: "diceroll",
    value: [Number(numOfDice), Number(numOfSides)],
  })
);
```

Given 3d100, will return:

```
 {
     type: 'diceroll'
     value: [3, 100]
 }
```
